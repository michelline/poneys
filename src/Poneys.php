<?php 
class Poneys {
	private $count ;

    /**
     * Poneys constructor.
     * @param $count
     */
    public function __construct($count = 8)
    {
        $this->count = $count;
    }

	public function getCount() {
		return $this->count;
	}

    public function setCount($count) {
        $this->count = $count;
    }

    public function addPoneyToField($number) {
        $this->count += $number;
    }

	public function removePoneyFromField($number) {
        if (($this->count-$number)<0) {
            throw new Exception('nombre négatif de poney');
        }
		$this->count -= $number;
	}

    public function getNames() {

    }

    public function isPlaceAvailable() {
        if ($this->count>15) {
            throw new Exception('Trop de poney dans le champ');
        }
        else if($this->count==15) {
            return false;
        }
        else if($this->count<15 && $this->count>=0) {
            return true;
        }
        else {
            throw new Exception('Nombre négatif de poney dans le champ');
        }
    }
}
?>
