<?php
require_once 'src/Poneys.php';

class MonTest extends \PHPUnit_Framework_TestCase {
    protected $poneys;

    public static function setUpBeforeClass() {

    }

    protected function setUp() {
        $this->poneys = new Poneys();
        $this->poneys->getCount(initPoneys);
    }

    protected function tearDown() {
        $this->poneys = null;
    }

    public static function tearDownAfterClass() {

    }

    public function test_placeAvailableInField() {
        // Action
        $this->assertTrue($this->poneys->isPlaceAvailable());
    }
}