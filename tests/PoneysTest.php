<?php
require_once 'src/Poneys.php';

class PoneysTest extends \PHPUnit_Framework_TestCase {

    protected $poneys;

    protected function setUp(){
        $this->poneys = new Poneys();
        $this->poneys->setCount(8);
    }

    protected function tearDown() {
        $this->poneys = null;
    }

    public function test_removePoneyFromField() {
		// Action
		$this->poneys->removePoneyFromField(3);

		// Assert
		$this->assertEquals(5, $this->poneys->getCount());
	}

    public function test_addPoneyToField() {
        // Action
        $this->poneys->addPoneyToField(1);

        // Assert
        $this->assertEquals(9, $this->poneys->getCount());
    }

    /**
     * @expectedException Exception
     */
    public function test_removeTooManyPoneyFromField() {
        // Setup
        $Poneys = new Poneys();

        // Action
        $Poneys->removePoneyFromField(11);
    }

    /**
     * @dataProvider provider_addPoneyToField
     */
    public function test_addPoneyToFieldWithDataProvider($count,$add,$test) {
        // Setup
        $Poneys = new Poneys($count);

        // Action
        $Poneys->addPoneyToField($add);

        // Assert
        $this->assertEquals($test, $Poneys->getCount());
    }

    public function provider_addPoneyToField()
    {
        return array(
            array(1, 0, 1),
            array(8, 2, 10),
        );
    }

    public function test_printPoneysNames()
    {
        // Create a stub for the SomeClass class.
        $stub = $this->getMockBuilder('Poneys')
            ->getMock();

        // Configure the stub.
        $stub->method('getNames')
            ->willReturn(array(
                'foo',
                'faa',
            ));

        // Calling $stub->doSomething() will now return
        // 'foo'.
        $this->assertSame(array(
            'foo',
            'faa',
        ), $stub->getNames());
    }

    public function test_placeAvailableInField() {
        // Setup
        $Poneys = new Poneys(15);

        // Action
        $this->assertNotTrue($Poneys->isPlaceAvailable());
    }

    public function test_placeNotAvailableInField() {
        // Setup
        $Poneys = new Poneys(10);

        // Action
        $this->assertTrue($Poneys->isPlaceAvailable());
    }
}
?>
